/*
|----------------------------------------------
| setting up server entry point
| @author: jahid haque <jahid.haque@yahoo.com>
| @copyright: riz, 2018
|----------------------------------------------
*/
require('dotenv').config({ slient: true});

const express = require('express');
const hbs = require('express-handlebars');
const Path = require('path');
const PublicRouter = require('./app_server/route/index');
const MarketRouter = require('./app_server/route/market');
const ApiRouter = require('./app_server/route/api');

// db connection.
require('./app_server/model/db');

const app = express();

// setting up templating engine.
app.engine('hbs', hbs({
    extname: 'hbs',
    defaultLayout: 'master',
    layoutsDir: Path.resolve(__dirname, './public/views/layouts'),
    partialsDir: Path.resolve(__dirname, './public/views/partials'),
}));

app.set('views', Path.resolve(__dirname, './public/views'));
app.set('view engine', 'hbs');

app.use(express.static(Path.join(__dirname, '/public')));

app.use('/market', MarketRouter);
app.use('/api', ApiRouter);
app.use('/', PublicRouter);

app.listen(process.env.port, () => {
    console.log('application running on port\t' + process.env.port);
});
