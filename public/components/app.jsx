/*
|----------------------------------------------
| font-end application entry point
| @author: jahid haque <jahid.haque@yahoo.com>
| @copyright: 4riz, 2018
|----------------------------------------------
*/

import { Component } from 'react';
import { render } from 'react-dom';
import Footer from './static/footer.jsx';
import Nav from './static/nav.jsx';
import Home from './home/controller.jsx';
import Price from './price/controller.jsx';

class App extends Component {

    state = {
        path: window.location.pathname,
        component: null
    };

    componentDidMount() {
        let path = this.state.path.replace('/', '');
        let component;

        switch(path) {
            case 'price': 
                component = <Price />;
                break;
            default: 
                component = <Home />;
        }

        this.setState({ component });
    }

    render() {        
        const View = this.state.path === '/price' ? <Price /> : null;
        return (
            <React.Fragment>
                <Nav />
                    {
                        this.state.component
                    }
                <Footer />
            </React.Fragment>
        )
    }
}

render(<App />, document.getElementById('bootstrap'));
