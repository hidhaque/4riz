/*
|----------------------------------------------
| setting up common footer for the site.
| @author: jahid haque <jahid.haque@yahoo.com>
| @copyright: 4riz, 2018
|----------------------------------------------
*/

import { Component } from 'react';

const Footer = () => {
    return (
        <div className  = 'footer'>
            <p className ='mb-0'>All &copyright; reserved, 2018</p>
        </div>
    )
}
export default Footer;
