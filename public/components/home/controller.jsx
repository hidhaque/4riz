/*
|----------------------------------------------
| font-end home entry point
| @author: jahid haque <jahid.haque@yahoo.com>
| @copyright: 4riz, 2018
|----------------------------------------------
*/

import React, { Component } from 'react';

class Home extends Component {
    render() {
        return (
            <h1>home page</h1>
        )
    }
};

export default Home;
