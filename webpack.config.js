/*
|----------------------------------------------
| setting up webpack for the app
| @author: jahid haque <jahid.haque@yahoo.com>
| @copyright: riz, 2018
|----------------------------------------------
*/

const Path = require('path');
const Webpack = require('webpack');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
    entry: {
        app: Path.resolve(__dirname, './public/components/app.jsx'),
    },
    output: {
        path: Path.resolve(__dirname, './public/js'),
        filename: '[name].js',
        publicPath: '/js',
    },

    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_module/,
                use: {
                    loader: 'babel-loader',
                },
            },
        ],
    },

    plugins: [
        new UglifyJSPlugin({
            uglifyOptions: {
                sourceMap: true,
                parallel: true,
                compress: {},
                extractComments: true,
                sourceMap: true
            }            
        }),
        new CleanWebpackPlugin('./public/js/', {}),
    ],
    externals: {
        // Use external version of React and ReactDOM
        "react": "React",
        "react-dom": "ReactDOM"
    },
}