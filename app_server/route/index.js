/*
|----------------------------------------------
| setting up public routes
| @author: jahid haque <jahid.haque@yahoo.com>
| @copyright: riz, 2018
|----------------------------------------------
*/

const Express = require('express');

const router = Express.Router();

router.get('/', (req, res) => {
    res.render('index');
});

router.get('/price', (req, res) => {
    res.render('index');
});

module.exports = router;
