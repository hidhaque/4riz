/*
|----------------------------------------------
| setting up public routes for market
| @author: jahid haque <jahid.haque@yahoo.com>
| @copyright: riz, 2018
|----------------------------------------------
*/

const Express = require('express');

const router = Express.Router();

router.get('/', (req, res) => {
    res.render('market');
});

module.exports = router;
